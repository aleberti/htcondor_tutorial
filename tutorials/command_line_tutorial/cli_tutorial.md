# INTRODUCTION TO HTCONDOR

HTCondor is a batch system through which users can submit, monitor and remove jobs. When you submit a job from a submit machine, it will be queued by the HTCondor queue system, which checks the requirements and priority of the job. The batch system’s central manager will then assign the job to run in a worker node (WN) matching the requirements specified by the job, if any. After execution is completed, the job log, standard error and standard output are retrieved and therefore visible in the submit machine.

To understand more about how HTCondor matches requirements and priority of jobs, have a look at ClassAds (https://research.cs.wisc.edu/htcondor/classad/classad.html).

# Submitting a job

To run a job in HTCondor, the following steps are necessary:
1. prepare your program to be run as a background batch job (i.e. no interactive input and output). Therefore, if the program needs some input, it should be given in some way e.g. as a configuration file or with command line options and/or arguments
2. prepare the so called submission file (in HTCondor jargon, ‘submit description file’), where the details of your job are contained
3. submit the job
4. monitor and manage the job
5. inspect the output of your finished job

Most of the tutorial is devoted to point 2 i.e. preparing the submission file covering different cases. The first example below shows a very basic working submission file, with an explanation of the HTCondor commands to submit, monitor and remove jobs.

You can find more information about submitting a job in the HTCondor documentation:
* https://htcondor.readthedocs.io/en/latest/users-manual/submitting-a-job.html
* https://htcondor.readthedocs.io/en/latest/man-pages/condor_submit.html

## Example 1 (Basic job submission, monitoring and removal)

A very simple and basic example of submission file is the following (called `example1.sub`):

```
##########################################
# Example 1
# Basic HTCondor submit description file
# to submit one job
##########################################

universe = vanilla
executable = example1.sh
arguments = 60
output = example1.out
error = example1.err
log = example1.log

queue
```


where `example1.sh` content is:

```bash
#!/bin/bash

/bin/sleep $@
```


The submission file above contains all the basic ingredients needed to submit an HTCondor job. A submission file is nothing else than a list of commands specifying what our job should run and, if any, the requirements of the machine where it should run (CPU, memory, if GPU is needed etc.). Let’s examine the commands of `example1.sub` one by one:
* first we see a header with `#` characters: HTCondor will not use this part, as any line starting with `#` is considered a comment
* universe = vanilla : the `universe` command specifies a HTCondor execution environment, called universe. The `vanilla` universe is fine for most programs; in principle it is also the default universe, but it depends on the configuration of the HTCondor pool. Therefore, it is better to specify which universe to use. To see more about universes, have a look at https://htcondor.readthedocs.io/en/latest/users-manual/choosing-an-htcondor-universe.html
* executable = &lt;executable_name&gt; : the name of the program to be run by the job. The program name can be specified with an absolute or relative path (relative to the working directory defined by where the job is submitted), or just with its name like in the example, which means the program is in the same directory from where the job will be submitted. This command is mandatory in every submission file
* arguments = &lt;args_list&gt; : a list of arguments to be passed to our program, in this case to `example1.sh`. Of course, the arguments list should be a valid one for our program
* output = &lt;output_file&gt; : it specifies the name of a file which will contain whatever the program would normally output on stdout (i.e. it is like a redirection of stdout to a file e.g. `cat myfile.txt > myfilecopy.txt` ). output_file can be given with its full path, relative to the machine from which the job is submitted. If just the name is specified, it will end up in the directory from which the job was submitted. The file is created in the scratch directory in the working node machine where the job is executed, then at completion it is transferred back to the specified location. **NB**: if a path is specified (absolute or relative), the directory where the file should be put should already exist, HTCondor will not create it for you.
* error = &lt;error_file&gt; : like output, but it writes in error_file what is written to stderr
* log = &lt;log_name&gt; : with this command you can specify the name (the path is optional) of a file where information about the job (when it starts running, where it is running, when it completes etc.) is written. This is useful in the case there is something wrong with the job, and you can inspect this file to understand what happened
* queue : this is the command telling to submit 1 copy of the job described by the submission file to the HTCondor queue. This command is mandatory and should be the last one in the submission file. In its most basic form, the queue command takes an optional integer argument e.g. queue 10, which will submit 10 copies of the job specified.

Now that the submission file is ready, the job can be submitted. Before submitting the job, it is good practice to run the executable with the arguments given in the submission file, to check that it executes flawlessly (NB: remember to add execution permission to the bash script!):

```
[you@your_terminal] $ ./example1.sh 60
```


To submit the job, HTCondor provide the `condor_submit` command:

```
[you@your_terminal] $ condor_submit example1.sub
```


To submit at CNAF, some more options need to be added:

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example1.sub
Submitting job(s).
1 job(s) submitted to cluster 2308938.
```


where `-spool` is used to spool input and output file in the worker node and `-name` specifies the name of the scheduler to be used for the job. When a job is submitted, a cluster (or batch) ID is assigned to it, in this case it is equal to 2308938. When multiple jobs are submitted with the same submission file, they will share the same cluster ID, but each of them will have a unique process ID, which is assigned in increasing order starting from 0. That way, each job can be uniquely specified with the cluster ID and process ID, also called job ID.

To monitor the status of HTCondor jobs, one can use the `condor_q` command, specifying the scheduler node:

```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it

-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 03/23/20 19:49:41
OWNER  BATCH_NAME       SUBMITTED   DONE   RUN    IDLE  TOTAL JOB_IDS
aberti ID: 2252156     3/22 20:17      _      _      _      1 2252156.0
aberti ID: 2252770     3/22 20:21      _      _      _      1 2252770.0
aberti ID: 2253313     3/22 20:24      _      _      _      1 2253313.0
aberti ID: 2260619     3/22 21:14      _      _      _      1 2260619.0
aberti ID: 2263105     3/22 21:31      _      _      _      4 2263105.0-3
aberti ID: 2306414     3/23 11:52      _      _      _      2 2306414.0-1
aberti ID: 2306416     3/23 11:59      _      _      _      2 2306416.0-1
aberti htcondor_test   3/23 12:14      _      _      _      4 2306425.0 ... 2307015.1
aberti ID: 2308934     3/23 19:16      _      _      _      1 2308934.0
aberti ID: 2308936     3/23 19:23      _      _      _      1 2308936.0
aberti ID: 2308938     3/23 19:48      _      1      _      1 2308938.0

Total for query: 19 jobs; 18 completed, 0 removed, 0 idle, 1 running, 0 held, 0 suspended
Total for all users: 45322 jobs; 44844 completed, 2 removed, 54 idle, 421 running, 1 held, 0 suspended
```


Since we submitted just one job (the last in the list), its process ID is 0, so that the complete job ID is 2308938.0. We can also see that some jobs have more processes e.g. for the batch 2263105 there are 4 jobs with process IDs from 0 to 3.

From the previous output, not much information is shown. One can then try to check the status of a specific job:

```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -better-analyze 2308938

-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it
The Requirements expression for job 2308938.000 is

    (TARGET.Arch == "X86_64") && (TARGET.OpSys == "LINUX") && (TARGET.Disk >= RequestDisk) && (TARGET.Memory >= RequestMemory) &&
    ((TARGET.FileSystemDomain == MY.FileSystemDomain) || (TARGET.HasFileTransfer))

Job 2308938.000 defines the following attributes:

    DiskUsage = 17
    FileSystemDomain = "ui-tier1.cr.cnaf.infn.it"
    ImageSize = 275
    MemoryUsage = ((ResidentSetSize + 1023) / 1024)
    RequestDisk = DiskUsage
    RequestMemory = ifthenelse(MemoryUsage =!= undefined,MemoryUsage,(ImageSize + 1023) / 1024)
    ResidentSetSize = 275

The Requirements expression for job 2308938.000 reduces to these conditions:

         Slots
Step    Matched  Condition
-----  --------  ---------
[0]        8977  TARGET.Arch == "X86_64"
[1]        8977  TARGET.OpSys == "LINUX"
[3]        8977  TARGET.Disk >= RequestDisk
[5]        8977  TARGET.Memory >= RequestMemory
[8]        8977  TARGET.HasFileTransfer


2308938.000:  Job is completed.

Last successful match: Mon Mar 23 19:49:30 2020


2308938.000:  Run analysis summary ignoring user priority.  Of 415 machines,
      0 are rejected by your job's requirements
    251 reject your job because of their own requirements
      0 match and are already running your jobs
      0 match but are serving other users
    164 are able to run your job
```


Here we can see that the output shows the requirements of our job, its status (completed) and finally how many machines could meet the requirements and therefore be able to run the job.

But there is another way to see information on the execution of the job, that is looking inside the log file specified by the `log` command in the submission file. The problem is that the files specified by the commands `log`, `output` and `error` are not in the directory from which the job was submitted. The reason is that at CNAF one has to transfer those manually after the job completes. To do this, one can run the `condor_transfer_data` specifying the cluster ID for the job(s) we want to retrieve the data:

```
[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2308938
Fetching data files...
```


After that we can see that the log, output and error files were moved in the current directory. Let's have a look to the log file `example1.log`:

```
[you@your_terminal] $ cat example1.log
000 (2308938.000.000) 03/23 19:48:27 Job submitted from host: <131.154.192.58:9618?addrs=131.154.192.58-9618&noUDP&sock=2569951_3482_3>
...
001 (2308938.000.000) 03/23 19:49:31 Job executing on host: <131.154.194.147:9618?addrs=131.154.194.147-9618&noUDP&sock=24391_c695_3>
...
006 (2308938.000.000) 03/23 19:49:40 Image size of job updated: 260
        1  -  MemoryUsage of job (MB)
        260  -  ResidentSetSize of job (KB)
...
005 (2308938.000.000) 03/23 19:50:32 Job terminated.
        (1) Normal termination (return value 0)
                Usr 0 00:00:00, Sys 0 00:00:00  -  Run Remote Usage
                Usr 0 00:00:00, Sys 0 00:00:00  -  Run Local Usage
                Usr 0 00:00:00, Sys 0 00:00:00  -  Total Remote Usage
                Usr 0 00:00:00, Sys 0 00:00:00  -  Total Local Usage
        0  -  Run Bytes Sent By Job
        26  -  Run Bytes Received By Job
        0  -  Total Bytes Sent By Job
        26  -  Total Bytes Received By Job
        Partitionable Resources :    Usage  Request Allocated
           Cpus                 :        0        1         1
           Disk (KB)            :       17        1   1548836
           Memory (MB)          :        1        1       128
...
```


Here we can see when the job was submitted, when it started to be executed and when it completed.

Finally, one can use the `-nobatch` to see individual jobs not grouped by batches:

```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 03/24/20 18:33:37
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE CMD
2252156.0   aberti          3/22 20:17   0+00:00:03 C  0    0.0 sh sleep 60
2252770.0   aberti          3/22 20:21   0+00:00:03 C  0    0.0 sh sleep 60
2253313.0   aberti          3/22 20:24   0+00:01:02 C  0    1.0 sleep 60
2260619.0   aberti          3/22 21:14   0+00:01:04 C  0    1.0 sleep 60
2263105.0   aberti          3/22 21:31   0+00:01:02 C  0    1.0 sleep 60
2263105.1   aberti          3/22 21:31   0+00:01:02 C  0    1.0 sleep 60
2263105.2   aberti          3/22 21:31   0+00:01:12 C  0    1.0 sleep 70
2263105.3   aberti          3/22 21:31   0+00:01:12 C  0    1.0 sleep 70
2306414.0   aberti          3/23 11:52   0+00:01:02 C  0    1.0 sleep 60
2306414.1   aberti          3/23 11:52   0+00:01:12 C  0    1.0 sleep 70
2306416.0   aberti          3/23 11:59   0+00:01:02 C  0    1.0 sleep 60
2306416.1   aberti          3/23 11:59   0+00:01:13 C  0    1.0 sleep 70
2306425.0   aberti          3/23 12:14   0+00:01:03 C  0    1.0 sleep 60
2306425.1   aberti          3/23 12:14   0+00:01:12 C  0    1.0 sleep 70
2307015.0   aberti          3/23 12:37   0+00:01:03 C  0    1.0 sleep 60
2307015.1   aberti          3/23 12:37   0+00:01:13 C  0    1.0 sleep 70
2308934.0   aberti          3/23 19:16   0+00:00:01 C  0    0.0 example1.sh -c 1 -t 60
2308936.0   aberti          3/23 19:23   0+00:00:01 C  0    0.0 example1.sh -c 1 -t 60
2308938.0   aberti          3/23 19:48   0+00:01:02 C  0    1.0 example1.sh 60

Total for query: 19 jobs; 19 completed, 0 removed, 0 idle, 0 running, 0 held, 0 suspended
Total for all users: 50142 jobs; 47536 completed, 2 removed, 1397 idle, 1207 running, 0 held, 0 suspended
```

We can see that some other information is given here, like the submission time, the execution time, the status of the jobs ("C" stands for "completed") and the command that was run with the jobs.

Finally, what if we want to cancel our job? The answer is using the `condor_rm` command. At CNAF you would do:

```
[you@your_terminal] $ condor_rm -name sn-01.cr.cnaf.infn.it 2320191
All jobs in cluster 2320191 have been marked for removal
```

With this, the very basic submission, monitoring ans removing jobs with HTCondor have been introduced. The following examples will play around this first example, adding more and more functionality.

## Exercise 2 (Cluster ID and Process ID; naming job batches)

In example 1 we saw that when submitting a job, or multiple ones at a time, a cluster ID and a process ID is assigned, so that a job can be uniquely identified as a clusterID.processID pair. This feature can be exploited to have also unique names for the log, error and output files. Let's see how.

In the submission file the user can define variables as `<variable_name> = <string>`, similarly to how commands are defined. After a variable has been defined, its value can be retrieved with the macro `$(variable_name)`.

While the user can define variables within the submission file, there are some variables which are defined automatically by HTCondor. Two of these automatic variables are `$(ClusterID)` and `$(ProcID)`, whose value is defined at submission time and assigned to the cluster ID and process ID of the job submitted.

In addition, we can give a name to the job batch with the `JobBatchName` command.

`example2.sub` (the header comments are not shown) shows how to use HTCondor automatic variable for the cluster and process IDs and how to name the job batch.

```
universe = vanilla
executable = example2.sh
arguments = 60
output = example2_$(ClusterId).$(ProcId).out
error = example2_$(ClusterId).$(ProcId).err
log = example2_$(ClusterId).$(ProcId).log

JobBatchName="Example_2"

queue 2
```

where `example2.sh` is:

```bash
#!/bin/bash

/bin/sleep $@
```

Therefore we expect to have 2 identical jobs submitted with the same cluster ID, but different process IDs (0 and 1). Moreover, the job batch will be named "Example_2". Let's submit the job.

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example2.sub
Submitting job(s)..
2 job(s) submitted to cluster 2491276..
```


If we monitor the submission we will see our job batch indentified by the name we specified in the submission file (the ouptut is cut to show only the jobs of interest):

```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/02/20 10:44:19
OWNER  BATCH_NAME                     SUBMITTED   DONE   RUN    IDLE  TOTAL JOB_IDS
aberti Example_2                     4/2  10:44      _      _      2      2 2491276.0-1
```


As predicted, the jobs appear with the batch name "Example_2" and their job ids are 2491276.0 and 2491276.1 (last column), as expected. If instead we add the `-nobatch` option we will see the two jobs independently:


```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/02/20 10:44:29
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE  CMD
2491276.0   aberti          4/2  10:44   0+00:00:00 I  0     0.0 example2.sh 60
2491276.1   aberti          4/2  10:44   0+00:00:00 I  0     0.0 example2.sh 60
```

One can repeat the same exercise queueing more than 2 jobs, that is fine as long as the arguments of our program do not change. To submit multiple jobs with different arguments (e.g. input files, command line arguments or options), one has to use the different forms of the `queue` command, as described in exercises 5, 6, 7 and 8.

## Exercise 3 (Job requests)

As stated in the introduction paragraph, HTCondor checks for the requirements (e.g. number of CPU, amount of memory etc.) of the jobs in the queue and uses them to see which machines are able to meet those requirements. Therefore, there should a way to require specific requirements for our jobs. As done till now, this is done in the submission file. Let's look at another example, `example3.sub`:

```
universe = vanilla
executable = example3.sh
arguments = 60
output = example3_$(ClusterId).$(ProcId).out
error = example3_$(ClusterId).$(ProcId).err
log = example3_$(ClusterId).$(ProcId).log

request_memory = 4 GB
request_cpus = 8
request_disk = 30 GB

JobBatchName="Example_3"

queue

```

Here we see three new commands:
* `request_memory`: it is used to set a requirement on the amount of RAM needed by the job; a unit (GB, kB) is needed
* `request_cpus`: it specifies the number of CPUs for the job
* `request_disk`: sets a requirement for the amount of disk space needed by the job; a unit (GB, kB) is needed.

Therefore in `example3.sub` we require that the machine that will run the job must have 8 CPUs, reserve 4 GB of RAM and 30 GB of disk. The default values of those requirements, if not set in the submission file, are 1 CPU, 2 GB of memory per CPU and 15 GB of disk per CPU. Let's submit the job:

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example3.sub
Submitting job(s).
1 job(s) submitted to cluster 2492323.
```

Let's monitor the job to see its requirements after it has been submitted:

```
[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it 2492323 -af RequestCpus RequestMemory RequestDisk
8 4096 31457280
```

The result is exactly what it was required in the submission file, except that the size of RAM is in MB while the disk size is in kB. In the last command we used the `-af` (abbreaviation for `-autoformat`) option of the `condor_q` command, through which we can see formatted output in a default way for the attributes listed after `-af`. In our case, we want to display the values of `RequestCpus`, `RequestMemory` and `RequestDisk`, which are the ones we are interested in. As an example, we can check the job batch name, cluster and process ids by specifyin the attributes `JobBatchName`, `ClusterId` and `ProcId` attributes after the `-af` option.


## Exercise 4 (Environment)

Sometimes one needs to define environment variables for their jobs. When submitting a job, `HOME` is not defined, while `PATH` is a generic path like `/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin`. HTCondor gives the possibility to define environment variables in the submission file, or to retrieve the value of locally defined environment variables. One can define environment variables in submission files using the `environment` command, which has the following signature:

```
environment = ENV1=... ; ENV2=...; ... ENVn=...
```

If the user wants to retrieve the value of an environment variable defined locally, the `$ENV(EnvironmentVariableName)` macro can be used.

To understand better, let's check `example4.sub`:

```
universe = vanilla
executable = example4.sh
output = example4_$(ClusterId).$(ProcId).out
error = example4_$(ClusterId).$(ProcId).err
log = example4_$(ClusterId).$(ProcId).log

environment = SOFT_DIR=/software/cnaf; OTHER_DIR=/home/cnaf; PATH=$ENV(PATH)

queue
```

where the content of `example4.sh` is:

```bash
#!/bin/bash

echo "Predefined environment:"
echo "My \$HOME: $HOME"
echo "Environment in submit file:"
echo "My \$SOFT_DIR: $SOFT_DIR"
echo "My \$OTHER_DIR: $OTHER_DIR"
echo "My \$PATH: $PATH"
echo "Environment in the script:"
export TEST_DIR=$HOME/test
echo "My \$TEST_DIR: $TEST_DIR"
```

From the submission file, we expect `SOFT_DIR` to be equal to `/software/cnaf`, `OTHER_DIR` to `/home/cnaf`, while `PATH` will have the same value as the `PATH` defined in the submission machine.

Let's submit the job and retrieve the output, log and error files. We then check the output file:

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example4.sub
Submitting job(s).
1 job(s) submitted to cluster 2492450.

[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2492450.0
Fetching data files...

[you@your_terminal] $ cat example4_2492450.0.out
Predefined environment:
My $HOME:
Environment in submit file:
My $SOFT_DIR: /software/cnaf
My $OTHER_DIR: /home/cnaf
My $PATH: /usr/lib64/qt-3.3/bin:/usr/share/lsf/9.1/linux2.6-glibc2.3-x86_64/etc:
/usr/share/lsf/9.1/linux2.6-glibc2.3-x86_64/bin:/bin:/usr/condabin:/usr/local/bin:
/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin
Environment in the script:
My $TEST_DIR: /test
```

As said before, `HOME` is not defined, so `TEST_DIR` is equal to `/test`. `SOFT_DIR` and `OTHER_DIR` have the value defined in the submission file. For `PATH`, we check its value in the submission machine:

```
[you@your_terminal] $ echo $PATH
/usr/lib64/qt-3.3/bin:/usr/share/lsf/9.1/linux2.6-glibc2.3-x86_64/etc:
/usr/share/lsf/9.1/linux2.6-glibc2.3-x86_64/bin:/bin:/usr/condabin:
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin
```

Therefore, the local `PATH` was correctly retrieved in the HTCondor job.

Another way to achieve the result of the `environment` command is to use exports in the bash script, therefore using the `export` command. Depending on your application, you may prefer one or the other method.


## Exercise 5 (Using the queue command: multiple queue statements)

As seen in example 2, one can submit more identical jobs with the `queue` command followed by an integer denoting the number of copies of the job to be submitted. Of course, this approach has the limitation that we cannot submit jobs with e.g. different arguments. For this purpose, the `queue` command can be used in different way and with different arguments. Here we see the easiest way to use the `queue` command to submit jobs using the same submission file but passing different argument for each job to the bash script.

Let's see `example5.sub`:

```
universe = vanilla
executable = example5.sh
output = example5_$(ClusterId).$(ProcId).out
error = example5_$(ClusterId).$(ProcId).err
log = example5_$(ClusterId).$(ProcId).log

JobBatchName="Example_5"

arguments = 60
queue

arguments = 80
queue

```

where `example5.sh` is:

```bash
#!/bin/bash

/bin/sleep $@
```

We can see that `example5.sub` contains two `arguments` commands, each followed by a `queue` command. This means that two jobs will be submitted, but we can change the value of the other command between `queue` commands to have jobs submitted with different settings. In this particular case, one job will have argument equal to 60, the second to 80.

Let's check if it is actually the case, so let's submit the job and monitor the job without the batch grouping (`-nobatch`):

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example5.sub
Submitting job(s)..
2 job(s) submitted to cluster 2492482.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/02/20 22:22:59
2492482.0   aberti          4/2  22:22   0+00:00:00 I  0     0.0 example5.sh 60
2492482.1   aberti          4/2  22:22   0+00:00:00 I  0     0.0 example5.sh 80
```

Therefore we can see that two jobs were submitted and, as predicted, the argument of the first is 60, while it is 80 for the second.

From this example we understand that using multiple `queue` commands can be useful when a single (or just some) characteristics are changing between jobs. However, HTCondor defines other three ways to use the `queue` command, as shown in the following examples.

## Exercise 6 (Using the queue command: matching pattern)

The second way of using the `queue` command uses patterns matching. The signature to use the command in this form is:

```
queue [<int expr> ] [<varname> ] matching [files | dirs ] [slice ] <list of items with file globbing>
```

Let's see a real example to understand how to use `queue` in this form through `example6.sub`:

```
universe = vanilla
executable = /bin/cat
arguments = $(filename)
output = example6_$(ClusterId).$(ProcId).out
error = example6_$(ClusterId).$(ProcId).err
log = example6_$(ClusterId).$(ProcId).log

should_transfer_files = yes
transfer_input_files = Hello1, Hello2, Hello3

JobBatchName="Example_6"

queue filename matching files Hello*
```

In the same directory of `example6`, we have three files named `Hello1`, `Hello2` and `Hello3` respectively. Each of them has a different content:

```
[you@your_terminal] $ grep Hello Hello*
Hello1:Hello first world!
Hello2:Hello second world!
Hello3:Hello third world!
```

Now let's examine the submission file to understand what will happen. First, the `queue` command: the meaning of the command in this case is: for each file (denoted by the keyword `files`) matching (denoted by the keyword `matching`) the pattern `Hello*`, assign the value of the variable `filename` (the name here is defined by the user) and submit one job. The pattern will be resolved using file globbing, therefore if wildcards are used, a list will be created. In the specific case, the pattern `Hello*` will resolve to the list of files `Hello1`, `Hello2` and `Hello3`. Moreover, we see that the `filename` variable is used as the argument of the executable, `cat` in this case. Therefore, the `filename` variable is defined in the `queue` command and then used to define the value of the `argument` command.

To understand even better what happens, we can write `example6.sub` using multiple `queue` statements. The commands:

```
arguments = $(filename)
queue filename matching files Hello*
```

are equivalent to:

```
arguments = Hello1
queue

arguments = Hello2
queue

arguments = Hello3
queue
```

It is clear from the previous equivalence that the pattern matching version of the `queue` command is very powerful, since it allows to replace multiple `queue` command with just one line, making submission files more concise, readable and helping avoiding mistakes.

Another new thing in `example6.sub` are the two lines:

```
should_transfer_files = yes
transfer_input_files = Hello1, Hello2, Hello3
```

which are simply telling to HTCondor to transfer the files specified by `transfer_input_files` to the directory where the jobs are run in the working node machine.

Finally, it is clear what will be the result of submitting `example6.sub`: three jobs will be submitted, each having a different file as argument to the `cat` command. We expect to find the content of the `Hello*` files in the output files of the jobs.

Let's submit the jobs and retrieve the output files to verify our prediction.

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example6.sub
Submitting job(s)...
3 job(s) submitted to cluster 2501354.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/03/20 09:54:23
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE  CMD
2501354.0   aberti          4/3  10:06   0+00:00:02 C  0     0.0 cat Hello1
2501354.1   aberti          4/3  10:06   0+00:00:01 C  0     0.0 cat Hello2
2501354.2   aberti          4/3  10:06   0+00:00:02 C  0     0.0 cat Hello3

[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2501354
Fetching data files...

[you@your_terminal] $ ls
example6_2501354.0.err  example6_2501354.1.log  example6_2501354.2.out  Hello3
example6_2501354.0.log  example6_2501354.1.out  example6.sub
example6_2501354.0.out  example6_2501354.2.err  Hello1
example6_2501354.1.err  example6_2501354.2.log  Hello2

[you@your_terminal] $ grep Hello example6_2501354.*.out
example6_2501354.0.out:Hello first world!
example6_2501354.1.out:Hello second world!
example6_2501354.2.out:Hello third world!
```

As expected, the output files of the jobs contain the content of the three `Hello` files.

At this point, let's examine the remaining optional items which were not used in the example, recalling the generic form of the pattern matching `queue` command:

```
queue [<int expr> ] [<varname> ] matching [files | dirs ] [slice ] <list of items with file globbing>
```

* `<int expr>`: used to specify the number of jobs to be submitted for each match. If not specified, it defaults to 1
* `dirs`: instead of matching files, it searches for matching directories. If neither `files` or `dirs` are used, the match will be performed for both files and directories.
* `slice`: it is a Python slice `[::]`, it can be used to select only some elements in the list of items. If not specified, all the items in the list will be taken. Negative steps (which are ok in Python) are not supported.

The next example shows how to use the `queue` command in yet another form.

## Exercise 7 (Using the queue command: from file)

Now we see how to use the `from file` version of the `queue` command. The signature of this version of the `queue` command is:

```
queue [<int expr> ] [<list of varnames> ] from [slice ] <file name> | <list of items>
```

Some of the items between square brackets were already seen in example 6. As before, let's use an example, `example7.sub`, to understand how this version of the `queue` command works.

```
universe = vanilla
executable = /bin/cat
arguments = $(option) $(filename)
output = example7_$(ClusterId).$(ProcId).out
error = example7_$(ClusterId).$(ProcId).err
log = example7_$(ClusterId).$(ProcId).log

should_transfer_files = yes
transfer_input_files = Hello1, Hello2, args.txt

JobBatchName="Example_7"

queue option, filename from args.txt
```

We see in the last line that the `queue` command needs a file called `args.txt` (the name is chosen by the user).

```
[you@your_terminal] $ cat args.txt
-n, Hello1
-E, Hello2
```

As before, the needed files, `Hello1`, `Hello2` and `args.txt` need to be transferred to the working node machine.

Now let's decode what the `queue` command is doing. In this case, the meaning is: for each line of the file `args.txt`, which are of the form `arg1, arg2`, take `arg1` and `arg2`, assign them to `option` and `filename` variables respectively (the names are defined by the user) and submit a job where `arguments` is composed by the values of `option` and `filename`. Therefore, looking at the content of `args.txt`, we expect 2 jobs to be submitted, one with the command `/bin/cat -n Hello1` and the other with `/bin/cat -E Hello2`. This means that the `queue` command of `example7.sub` is equivalent to:

```
option = -n
filename = Hello1
queue

option = -E
filename = Hello2
queue
```

Looking at the manual of the [`cat` manual](http://man7.org/linux/man-pages/man1/cat.1.html), we expect that the output file of the first job will contain the content of `Hello1` preceded by the line number, while the output of the second job will contain the content of `Hello2` followed by a `$`.

Let's submit the job, retrieve the output files and check if our predictions are correct.

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example7.sub
Submitting job(s)..
2 job(s) submitted to cluster 2502514.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/03/20 17:08:10
2502514.0   aberti          4/3  17:07   0+00:00:01 C  0     0.0 cat -n Hello1
2502514.1   aberti          4/3  17:07   0+00:00:02 C  0     0.0 cat -E Hello2

[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2502514
Fetching data files...

[you@your_terminal] $ ls
args.txt                example7_2502514.0.log  example7_2502514.1.err  example7_2502514.1.out  Hello1
example7_2502514.0.err  example7_2502514.0.out  example7_2502514.1.log  example7.sub            Hello2

[you@your_terminal] $ grep "Hello"  example7_2502514.*.out
example7_2502514.0.out:     1   Hello first world!
example7_2502514.1.out:Hello second world!$
```

Indeed, we were correct.

If we look back at the signature of this version of the `queue` command:

```
queue [<int expr> ] [<list of varnames> ] from [slice ] <file name> | <list of items>
```

As seen in the patern matching case, we can set different optional items whose meaning is the same (e.g. `int expr` or `slice`). We can see also that instead of having a file, a list of items can be supplied to fill the variables defined in `<list of varnames>`. This means that `example7.sh` could be rewritten as:

```
queue option, filename from (
    -n, Hello1
    -E, Hello2
)
```

Finally, it is needless to say that the number of options in the list contained in the file and the number of variables defined in the `queue` command should match.

Probably this version of the `queue` command is the most useful since:

* it supports multiple variables, as defined and needed by the user
* it is highly modular, so that one submit file can be used to submit many jobs

The only drawback is that an additional file is needed, but this can be omitted if the list is not that big. Even if an additional file is needed, it is a little price to pay to be able to use this powerful version of the `queue` command.


## Exercise 8 (Using the queue command: from list)

The last version of the `queue` command is the following:

```
queue [<int expr> ] [<varname> ] in [slice ] <list of items>
```

Given the previous versions of the command, it may be already clear how this version works, but let's see an example, `example8.sub`, anyway:

```
universe = vanilla
executable = example8.sh
arguments = $(arg1)
output = example8_$(ClusterId).$(ProcId).out
error = example8_$(ClusterId).$(ProcId).err
log = example8_$(ClusterId).$(ProcId).log

JobBatchName="Example_8"

queue arg1 in (60 50 30 20)
```

where `example8.sh` is the simple `sleep` command (as used in previous examples). Examining the `queue` command, we can understand that for each element in the list `(60 50 30 20)` a job will be submitted, its value is assigned to the variable `arg1` and finally to the `argument` command. Therefore, we expect to have 4 jobs submitted, giving 4 different arguments to `example8.sh`. This means that the `queue` command in `example8.sub` is equivalent to:

```
argument = 60
queue

argument = 50
queue

argument = 30
queue

argument = 20
queue
```

Let's submit the jobs and check them with `condor_q`:

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example8.sub
Submitting job(s)....
4 job(s) submitted to cluster 2507644.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/04/20 17:01:01
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE  CMD
2507644.0   aberti          4/4  17:00   0+00:01:02 C  0     1.0 example8.sh 60
2507644.1   aberti          4/4  17:00   0+00:00:52 C  0     1.0 example8.sh 50
2507644.2   aberti          4/4  17:00   0+00:00:32 C  0     1.0 example8.sh 30
2507644.3   aberti          4/4  17:00   0+00:00:22 C  0     1.0 example8.sh 20
```

As expected, 4 jobs were submitted, each assigning a different argument to `example8.sh` according to the element in the list provided with the `queue` command.

As the other versions of the `queue` command, the user can submit more copies of the jobs specifying an integer value or select specific element from the list using the slice `[::]`.

## Exercise 9 (Transferring files)

As already seen in previous examples, we can transfer input or output files to/from the working node directory where the jobs are running. By default, the output, error and log files (if defined) are transferred back to the submission directory when `condor_transfer` is used. For any other file created by our programs, we can decide which files should be transferred back or not.

Let's see an example, `example9.sub`:

```
universe = vanilla
executable = example9.sh
output = example9_$(ClusterId).$(ProcId).out
error = example9_$(ClusterId).$(ProcId).err
log = example9_$(ClusterId).$(ProcId).log

transfer_output_files=1.txt

queue
```

where `example9.sh` is:

```bash
#!/bin/bash

for i in {1..10}; do
    echo "Number $i" >> $_CONDOR_SCRATCH_DIR/1.txt
    sleep 2s
    echo "Hello" >> $_CONDOR_SCRATCH_DIR/2.txt
done
```

Let's examine `example9.sh` first. It loop ten times assigning at each cycle a different integer value to the variable `i`, from 1 to 10. For each cycle, it echoes a string to the file `$_CONDOR_SCRATCH_DIR/1.txt`. Here we notice the use of an environment variable called `_CONDOR_SCRATCH_DIR`: this is one of the environment variables defined by HTCondor which the user can use for the jobs. In particular, from HTCondor documentation:

* `_CONDOR_SCRATCH_DIR` gives the directory where the job may place temporary data files. This directory is unique for every job that is run, and its contents are deleted by HTCondor when the job stops running on a machine.

Therefore, this directory is in the working node machine where the job is running and it is unique for each job.

Coming back to `example9.sh`, the script proceeds sleeping for 2 seconds and then echoing another string to `$_CONDOR_SCRATCH_DIR/2.txt`. Therefore, in the scratch directory where `example9.sh` will run, two files will be created, called `1.txt` and `2.txt` respectively.

Now let's have a look to `example9.sub`. Everything is already known, except for one line: `transfer_output_files=1.txt`. However, the meaning is clear: among all the files created by the job, we want only `1.txt` to be transferred back when we will invoke `condor_transfer` (obviosuly, as said before, together with ouput, error and log files).

Let's submit the job and see what happens:

```
[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example9.sub
Submitting job(s).
1 job(s) submitted to cluster 2507694.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/04/20 17:55:04
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE  CMD
2507694.0   aberti          4/4  17:54   0+00:00:22 C  0     1.0 example9.sh

[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2507694
Fetching data files...

[you@your_terminal] $ ls
1.txt  example9_2507694.0.err  example9_2507694.0.log  example9_2507694.0.out  example9.sh  example9.sub

[you@your_terminal] $ cat 1.txt
Number 1
Number 2
Number 3
Number 4
Number 5
Number 6
Number 7
Number 8
Number 9
Number 10
```

After transferring back the files, we see that only 1.txt is there, as we expected.

There are few thing to note about the `transfer_output_files` command:
* it does not support wildcards, therefore if you want to transfer more files, a list should be provided
* if you mispell the name of a file, an empty file will be generated (since it is not a file present in the scratch directory)
* instead of files, directories can be specified. If a directory is specified, the whole directory with its files will be transferred back.

An example for the last point is given by the following:

```
#!/bin/bash
cd $_CONDOR_SCRATCH_DIR
mkdir my_files
cd my_files
for i in {1..10}; do
    echo "Number $i" >> ./1.txt
    sleep 2s
    echo "Hello" >> ./2.txt
done
```

where a directory called `my_files` is created. `1.txt` and `2.txt` are put within `my_files` directory. If we adapt `example9.sub` replacing `transfer_output_files=1.txt` with `transfer_output_files=my_files`, the whole directory will be transferred back, with both `1.txt` and `2.txt`. Still, if the user wants only `2.txt` to be transferred back, it should be specified with `transfer_output_files=my_files/2.txt` and the directory `my_files` will not be created on the submission directory.

You can try for yourself changing `example9.sub` and `example9.sh`.


## Exercise 10 (Transferring files with remapping)

Following the previous exercise, if the user wants to transfer the output file to another directory, without creating it in the working node scratch directory, the `transfer_output_remaps` command comes to the rescue. Let's see an example, `example10.sub`:

```
universe = vanilla
executable = example10.sh
output = example10_$(ClusterId).$(ProcId).out
error = example10_$(ClusterId).$(ProcId).err
log = example10_$(ClusterId).$(ProcId).log

transfer_output_files=1.txt
transfer_output_remaps="1.txt=outputs/1-$(ClusterId).$(ProcId).txt"

queue
```

and `example10.sh` is:

```bash
#!/bin/bash

for i in {1..10}; do
    echo "Number $i" >> $_CONDOR_SCRATCH_DIR/1.txt
    sleep 2s
    echo "Hello" >> $_CONDOR_SCRATCH_DIR/2.txt
done
```

So, as learned in example 9, only the file `1.txt` will be transferred back. But this time, instead of putting it in the submission directory, it will be put in the subdirectory `outputs` with a different name, which is built from the cluster and process ids of the job. It has to be noted that the directory where the file will be put must exist before transferring the data.

Let's submit the job and check what happens.

```
[you@your_terminal] $ mkdir outputs

[you@your_terminal] $ condor_submit -spool -name sn-01.cr.cnaf.infn.it example10.sub
Submitting job(s).
1 job(s) submitted to cluster 2507715.

[you@your_terminal] $ condor_q -name sn-01.cr.cnaf.infn.it -nobatch
-- Submitter: aberti@htc_tier1 : <131.154.192.58:9618?... : sn-01.cr.cnaf.infn.it @ 04/04/20 18:10:34
 ID         OWNER            SUBMITTED     RUN_TIME ST PRI SIZE  CMD
2507715.0   aberti          4/4  18:09   0+00:00:23 C  0     1.0 example10.sh

[you@your_terminal] $ condor_transfer_data -name sn-01.cr.cnaf.infn.it 2507715
Fetching data files...

[you@your_terminal] $ ls
example10_2507715.0.err  example10_2507715.0.log  example10_2507715.0.out  example10.sh  example10.sub  outputs

[you@your_terminal] $ ls outputs/
1-2507715.0.txt

[you@your_terminal] $ cd outputs/

[you@your_terminal] $ cat 1-2507715.0.txt
Number 1
Number 2
Number 3
Number 4
Number 5
Number 6
Number 7
Number 8
Number 9
Number 10
```

So, `1.txt` was created in the scratch directory where the job was running, then when the transfer is issued, its name is changed and then put in the directory `outputs`. 

This example closes the basic introduction to job submission with HTCondor. As stated in the introduction, you can find more info on this subject in the following links:
* https://htcondor.readthedocs.io/en/latest/users-manual/submitting-a-job.html
* https://htcondor.readthedocs.io/en/latest/man-pages/condor_submit.html